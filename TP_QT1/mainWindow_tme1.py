import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

"""
QUESTIONS TME 1
Q1: Il ne faut pas oublier la fonction super() qui hérite de la classe QMainWindow
Q2: Il faut créer une instance de QApplication puis l'exécuter
Q4: On connecte les actions au slots avec monAction.monSignal.connect(self.nomDuSlot)
Q6 : Il faudrait vérifier l'extension du fichier
"""

class MainWindow(QMainWindow):

    def __init__(self):
        #2e Etape: Créer une classe MainWindow
        super(MainWindow, self).__init__()
        print("Constructeur de la class MainWindow")
        
        #3e Etape: rajouter des widgets à MainWindow
        bar = self.menuBar()
        fileMenu = bar.addMenu("Fichier")
        
        act1 = QAction(QIcon("source/open.png"), "Open...", self)
        act1.setShortcut( QKeySequence("Ctrl+O") )
        act1.setToolTip("Open file")
        act1.setStatusTip("Open file")

        act2 = QAction(QIcon("source/save.png"), "Save...", self)
        act2.setShortcut( QKeySequence("Ctrl+S") )
        act2.setToolTip("Save file")
        act2.setStatusTip("Save file")

        act3 = QAction(QIcon("source/quit.png"), "Quit...", self)
        act3.setShortcut( QKeySequence("Ctrl+Q") )
        act3.setToolTip("Quit file")
        act3.setStatusTip("Quit file")

        fileMenu.addAction(act1)
        fileMenu.addAction(act2)
        fileMenu.addAction(act3)

        fileToolBar = QToolBar("Fichier")
        fileToolBar.addAction(act1)
        fileToolBar.addAction(act2)
        fileToolBar.addAction(act3)
        
        statusBar = QStatusBar()
        self.textEdit = QTextEdit()

        self.addToolBar(fileToolBar)  
        self.setStatusBar(statusBar)        
        self.setCentralWidget(self.textEdit)

        triggered = pyqtSignal()

        #4e Etape: définir et connecter les slots
        act1.triggered.connect(self.openFile)
        act2.triggered.connect(self.saveFile)
        act3.triggered.connect(self.quitFile)
        
    def closeEvent(self, event: QCloseEvent):
        event.ignore()
        

    

    @pyqtSlot()
    def openFile(self):
        #5e Etape: ouvrir une boîte de dialogue pour sélectionner un fichier
        print("Opening file...")
        fileName = QFileDialog.getOpenFileName(self, "Open File", "/home", "Text files(*.txt);; HTML files (*.html)")
        print(fileName)  
        #6e Etape: ouvrir / sauver une page HTML
        if (len(fileName[0]) != 0):
            file = open(fileName[0],"r")
            self.textEdit.setHtml(file.read())
            file.close()


    @pyqtSlot()
    def saveFile(self):
        #5e Etape: ouvrir une boîte de dialogue pour sélectionner un fichier 
        print("Saving file...")	
        fileName = QFileDialog.getSaveFileName(self, "Save File")
        print(fileName) 

        if (len(fileName[0]) != 0):
            file = open(fileName[0], "w")
            file.write(self.textEdit.toHTML())
            file.close()
        
	
    @pyqtSlot()
    def quitFile(self):
        #7e Etape: ouvrir une boîte de dialogue pour demander confirmation
        print("Quitting file..")	
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("Do you want to quit?")
        msg.setWindowTitle("Exit")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        ret = msg.exec_()
        if ret == QMessageBox.Yes:
            self.close()


def main(args):
    #print("Hello World")
    
    app = QApplication(args)
    win = MainWindow()
    
    win.show()
    app.exec_()
	

if __name__ == "__main__":
    main(sys.argv) 
    