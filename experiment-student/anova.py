


import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd
import pingouin as pg


if __name__=="__main__":
   # Load csv
   df = pd.read_csv("logs/results.csv")
   # Display the data with seaborn
   sns.barplot(data=df, x="ParticipantID", y="Time", hue ="Condition").set_title("Résultats de l'expérience.")
   
   # ANOVA test
   # the parameters are data=, dv=, wihtin=, subject=
   res = pg.rm_anova(data=df, dv="Time", within=["Condition", "Grid"], subject="ParticipantID" )
   print(res.to_string())

   # Posthoc test (if necessary)
   # the parameters are data=, dv=, wihtin=, subject=
   posthocs = pg.pairwise_ttests(dv='Time', within=['Condition', 'Grid'], subject='ParticipantID', data=df)
   pg.print_table(posthocs)



    

