package fr.upmc.votre_nom.sous;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    static final int CURRENCY_CHOOSER_REQUEST = 1;
    private EditText taux_echange_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        taux_echange_text = (EditText) findViewById(R.id.edit_taux_echange);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void convert(View sender){
        EditText euros_text = (EditText) findViewById(R.id.edit_euros);
        EditText resultat_text = (EditText) findViewById(R.id.edit_resultat);

        Double euro = Double.valueOf(euros_text.getText().toString());
        Double taux = Double.valueOf(taux_echange_text.getText().toString());

        Double res =  euro * taux;

        resultat_text.setText(String.valueOf(res));
    }

    public void choose_currency(View sender){
        Intent intent = new Intent(this, CurrencyChooserActivity.class);
        startActivityForResult( intent, CURRENCY_CHOOSER_REQUEST );
    }

    protected void onActivityResult( int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CURRENCY_CHOOSER_REQUEST) {
            if (resultCode == RESULT_OK) {
                String currency_from = data.getStringExtra("CURRENCY_FROM");
                String currency_to = data.getStringExtra("CURRENCY_TO");

                if(currency_from.equals("€") && currency_to.equals("€" )){
                    taux_echange_text.setText("1.0");
                }
                else{
                    taux_echange_text.setText("0.2");
                }
            }
        }
    }
}