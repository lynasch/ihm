package fr.upmc.votre_nom.sous;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

public class CurrencyChooserActivity extends AppCompatActivity {

    public static final String BUNDLE_EXTRA_CURRENCY_FROM = "CURRENCY_FROM";
    public static final String BUNDLE_EXTRA_CURRENCY_TO = "CURRENCY_TO";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_currency_chooser);

        RadioGroup currency_from = (RadioGroup) findViewById(R.id.currency_from);
        RadioGroup currency_to = (RadioGroup) findViewById(R.id.currency_to);

        Button convert_button = (Button) findViewById(R.id.convert);

        convert_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Integer id_from = currency_from.getCheckedRadioButtonId();
                Integer id_to = currency_to.getCheckedRadioButtonId();

                RadioButton chosen_from = (RadioButton) findViewById(id_from);
                RadioButton chosen_to = (RadioButton) findViewById(id_to);

                Intent res_intent = new Intent();

                res_intent.putExtra(BUNDLE_EXTRA_CURRENCY_FROM, chosen_from.getText().toString());
                res_intent.putExtra(BUNDLE_EXTRA_CURRENCY_TO, chosen_to.getText().toString());

                setResult(RESULT_OK, res_intent);
                finish();
            }
        });

    }
}