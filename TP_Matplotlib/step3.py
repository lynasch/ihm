import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.animation import *
from IPython.display import HTML
import matplotlib.animation as animation

# No toolbar
matplotlib.rcParams['toolbar'] = 'None'

# New figure with white background
fig = plt.figure(figsize=(6,6), facecolor='white')

# New axis over the whole figureand a 1:1 aspect ratio
# ax = fig.add_axes([0,0,1,1], frameon=False, aspect=1)
ax = fig.add_axes([0.005,0.005,0.990,0.990], frameon=True, aspect=1)

# Number of ring
n = 50
size_min = 50
size_max = 50*50
          
# Ring position 
POS = np.random.uniform(0,1,(n,2))

# Ring colors
COLOR = np.ones((n,4)) * (0,0,0,1)

# Alpha color channel goes from 0 (transparent) to 1 (opaque)
COLOR[:,3] = np.linspace(0,1,n)

# Ring sizes
SIZE = np.linspace(size_min, size_max, n)

# Scatter plot
scat = ax.scatter(POS[:,0], POS[:,1], s=SIZE, lw = 0.5,
                  edgecolors = COLOR, facecolors='None')

# Ensure limits are [0,1] and remove ticks
ax.set_xlim(0,1), ax.set_xticks([])
ax.set_ylim(0,1), ax.set_yticks([])


def animate(frame):
    global POS, COLOR, SIZE

    # Every ring is made more transparent. A ring is transparent after n frames
    # TODO
    COLOR[:,3] -= 1/n
    COLOR[:,3] = np.clip(COLOR[:, 3], 0, 1)

    # Each ring is made larger. size = size_max after n frames
    # TODO
    SIZE += (size_max-size_min)/n

    # Reset ring specific ring (relative to frame number)
    i = frame % n
    # TODO
    POS[i] = np.random.uniform(0, 1, 2)
    SIZE[i] = np.random.uniform(size_min, size_max)
    COLOR[i,3] = np.random.uniform(0, 1)

    # Update scatter object (scat)
    # use the methods set_edgecolors, set_sizes, set_offsets
    scat.set_edgecolors(COLOR)
    scat.set_sizes(SIZE)
    scat.set_offsets(POS)
    return scat,

# TODO
animation = animation.FuncAnimation(fig, animate, frames=100, blit=True, interval=20, repeat=True)

plt.show()