import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

fig1 = plt.figure(figsize=(10,6), facecolor='white')


#############################
#       Line plot        #
#############################
# show in 1x1 (row =1, column =1)
ax1 = plt.subplot(231)
X = np.linspace(-np.pi, np.pi, 256,endpoint=True)
C = np.cos(X)
S = np.sin(X)
plt.plot(X,C)

#############################
#       bar plot            #
#############################
# show in 1x2
# ax2 = plt.subplot(232)
n = 12
X = np.arange(n)
Y1 = (1-X/float(n)) * np.random.uniform(0.5,1.0,n)
Y2 = (2-X/float(n)) * np.random.uniform(0.5,1.0,n)
bar_width = 0.3
offset = 0.1

plt.axes( [0.4, 0.54, 0.52, 0.33])

plt.bar(X - offset- bar_width, Y1, width = bar_width, facecolor='#9999ff', edgecolor='white')
plt.bar(X + offset, Y2, width = bar_width, facecolor='#ff9999', edgecolor='white')

for x,y in zip(X,Y1):
    plt.text(x-offset-bar_width, y+0.15, '%.2f' % y, ha='center', va= 'top')

for x,y in zip(X,Y2):
    plt.text(x+offset, y+0.15, '%.2f' % y, ha='center', va= 'top')


plt.xlim(-1,n), plt.ylim(0,2)



#############################
#       scatter plot        #
#############################
# show in 2x1 
ax3 = plt.subplot(234)
n = 1024
X = np.random.normal(0,1,n)
Y = np.random.normal(0,1,n)
T = np.arctan2(Y,X)

#todo indicate where do you want do display this plot
plt.scatter(X,Y, s=75, c=T, alpha=.5) #todo





#############################
#       imshow plot         #
#############################
# show in 2x2
ax4 = plt.subplot(235)
def f(x,y):
    return (1-x/2+x**5+y**3)*np.exp(-x**2-y**2)

n = 10
x = np.linspace(-3,3,4*n)
y = np.linspace(-3,3,3*n)
X,Y = np.meshgrid(x,y)
Z = f(X,Y)

plt.imshow(Z,interpolation='bicubic', cmap='bone', origin='lower')
#plt.colorbar(shrink=.92)


#############################
#       Pie chart          #
#############################
# show in 2x3 
ax5 = plt.subplot(236)
n = 20
Z = np.ones(n)
Z[-1] *= 2
Z[-2] *= 3
plt.pie(Z, explode=Z*.05, colors=['%f' % (i/float(n)) for i in range(n)],
        wedgeprops={"linewidth": 1, "edgecolor": "black"})
plt.gca().set_aspect('equal')

ax3.set_xlim(-1.5,1.5), ax3.set_ylim(-1.5,1.5)
plt.show()