import numpy as np
import matplotlib.pyplot as plt
from matplotlib.animation import *
from IPython.display import HTML

fig = plt.figure(figsize=(6,3), dpi=100)

# Create a new subplot from a grid of 1x1
plt.subplot(111)

plt.xlim(( 0, 2))
plt.ylim((-2, 2))

line, = plt.plot([], [], lw=2)

# animation function. This is called sequentially
def animate(i):
    x = np.linspace(0, 2, 1000)
    y = np.sin(2 * np.pi * (x - 0.01 * i))
    line.set_data(x, y)
    return (line,)

# call the animator.
anim = FuncAnimation(fig=fig, func=animate, interval=20)

plt.show()