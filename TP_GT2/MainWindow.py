import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from Canvas import *
import resources

class MainWindow(QMainWindow):

    def __init__(self, parent = None):
        QMainWindow.__init__(self, parent)
        #[TME1]2e Etape: Créer une classe MainWindow
        print( "init mainwindow")
        self.resize(600, 500)

        #[TME1]3e Etape: rajouter des widgets à MainWindow
        bar = self.menuBar()
        fileMenu = bar.addMenu("File")

        act1 = QAction(QIcon(":/icons/open.png"), "Open...", self)
        act1.setShortcut( QKeySequence("Ctrl+O") )
        act1.setToolTip("Open file")
        act1.setStatusTip("Open file")

        act2 = QAction(QIcon(":/icons/save.png"), "Save...", self)
        act2.setShortcut( QKeySequence("Ctrl+S") )
        act2.setToolTip("Save file")
        act2.setStatusTip("Save file")

        act3 = QAction(QIcon(":/icons/quit.png"), "Quit...", self)
        act3.setShortcut( QKeySequence("Ctrl+Q") )
        act3.setToolTip("Quit")
        act3.setStatusTip("Quit")

        fileMenu.addAction(act1)
        fileMenu.addAction(act2)
        fileMenu.addAction(act3)

        fileToolBar = QToolBar("Fichier")
        fileToolBar.addAction(act1)
        fileToolBar.addAction(act2)
        fileToolBar.addAction(act3)
        self.addToolBar(fileToolBar)
        

        #[TME1]4e Etape: définir et connecter les slots
        triggered = pyqtSignal()
        act1.triggered.connect(self.openFile)
        act2.triggered.connect(self.saveFile)
        act3.triggered.connect(self.quitFile)
        
        colorMenu = bar.addMenu("Color")
        actPen = colorMenu.addAction(QIcon(":/icons/pen.png"), "&Pen color", self.pen_color, QKeySequence("Ctrl+P"))
        actBrush = colorMenu.addAction(QIcon(":/icons/brush.png"), "&Brush color", self.brush_color, QKeySequence("Ctrl+B"))
        
        colorToolBar = QToolBar("Color")
        self.addToolBar( colorToolBar )
        colorToolBar.addAction( actPen )
        colorToolBar.addAction( actBrush )
        
        
        #[TME2]6e Etape: tracer plusieurs formes géométriques
        shapeMenu = bar.addMenu("Shape")
        actRectangle = shapeMenu.addAction(QIcon(":/icons/rectangle.png"), "&Rectangle", self.rectangle )
        actEllipse = shapeMenu.addAction(QIcon(":/icons/ellipse.png"), "&Ellipse", self.ellipse)
        actFree = shapeMenu.addAction(QIcon(":/icons/free.png"), "&Free drawing", self.free_drawing)

        shapeToolBar = QToolBar("Shape")
        self.addToolBar( shapeToolBar )
        shapeToolBar.addAction( actRectangle )
        shapeToolBar.addAction( actEllipse )
        shapeToolBar.addAction( actFree )

        #[TME2]7 e Etape: Implementer plusieurs modes
        modeMenu = bar.addMenu("Mode")
        actMove = modeMenu.addAction(QIcon(":/icons/move.png"), "&Move", self.move)
        actDraw = modeMenu.addAction(QIcon(":/icons/draw.png"), "&Draw", self.draw)
        actSelect = modeMenu.addAction(QIcon(":/icons/select.png"), "&Select", self.select)

        actReset = bar.addAction("&Reset", self.reset)

        modeToolBar = QToolBar("Navigation")
        self.addToolBar( modeToolBar )
        modeToolBar.addAction( actMove )
        modeToolBar.addAction( actDraw )
        modeToolBar.addAction( actSelect )

        
        statusBar = QStatusBar()
        self.setStatusBar(statusBar) 

        self.textEdit = QTextEdit() 
        self.canva = Canvas()
        
        container = QWidget()
        self.setCentralWidget(container);
       
        v_layout = QVBoxLayout(container)
        v_layout.addWidget(self.canva)
        v_layout.addWidget(self.textEdit)
        
        
        mySlider = QSlider(Qt.Horizontal, self)
        mySlider.setGeometry(475, 30, 100, 30)
        mySlider.valueChanged[int].connect(self.changeValue)
        mySlider.valueChanged[int].connect(self.zoom)

     #[TME1]5e Etape: ouvrir une boîte de dialogue pour sélectionner un fichier
    @pyqtSlot()
    def openFile(self):
        print("Opening file...")
        fileName = QFileDialog.getOpenFileName(self, "Open File", "/home", "Text files(*.txt);; HTML files (*.html)")
        print(fileName)  
        
        #[TME1]6e Etape: ouvrir / sauver une page HTML
        if (len(fileName[0]) != 0):
            file = open(fileName[0], "r")
            self.textEdit.setHtml(file.read())
            file.close()


    @pyqtSlot()
    def saveFile(self):
        print("Saving file...")	
        fileName = QFileDialog.getSaveFileName(self, "Save File")
        print(fileName) 

        if (len(fileName[0]) != 0):
            file = open(fileName[0], "w")
            file.write(self.textEdit.toHtml())
            file.close
	
    #[TME1]7e Etape: ouvrir une boîte de dialogue pour demander confirmation
    @pyqtSlot()
    def quitFile(self):	
        self.close()
    
        
    #[TME1]8e Etape: demander confirmation dans tous les cas
    def closeEvent(self, event: QCloseEvent):
        reply = QMessageBox.question(self, 'Window Close', 'Do you want to quit ?', QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        if reply == QMessageBox.Yes:
            event.accept()
            print('Window closed')
        else:
            event.ignore()
            
    def reset(self):
        reply = QMessageBox.question(self, 'Reset', 'Are you sure you want to reset the canva ?', QMessageBox.Yes, QMessageBox.No)
        if reply == QMessageBox.Yes:
            self.canva.reset()
            print('reset Canva')
 

    def changeValue(self, value):
        self.log_action("slider on", value)
        print(value)
    
    def zoom(self, value):
        self.log_action("zoom")
        print(value)
        self.canva.zoomValue = value
        
    @pyqtSlot()
    def pen_color(self):
        self.log_action("choose pen color")
        color = QColorDialog().getColor()
        self.canva.set_color_pen(color)
    
    #[TME2]4e Etape: choisir interactivement les attributs graphiques
    @pyqtSlot()
    def brush_color(self):
        self.log_action("choose brush color")
        color = QColorDialog().getColor()
        self.canva.set_color(color)

    #[TME2]4e Etape: choisir interactivement les attributs graphiques
    @pyqtSlot()
    def rectangle(self):
        self.log_action("Shape mode: rectangle")
        self.canva.set_form(self.sender().text())
        
    #[TME2]4e Etape: choisir interactivement les attributs graphiques
    @pyqtSlot()
    def ellipse(self):
        self.log_action("Shape Mode: circle")
        self.canva.set_form(self.sender().text())

    def free_drawing(self):
        self.log_action("Shape mode: free drawing")
        self.log_action("Warning ! If you want to change colour for the free drawing, set the pen and not the brush.")
        self.canva.set_form(self.sender().text())

    @pyqtSlot()
    def move(self):
        self.log_action("Mode: move")
        self.canva.set_mode(self.sender().text())

    @pyqtSlot()
    def draw(self):
        self.log_action("Mode: draw")
        self.canva.set_mode(self.sender().text())
        
    @pyqtSlot()
    def select(self):
        self.log_action("Mode: select")
        print(self.sender().text())
        self.canva.set_mode(self.sender().text())
    
    @pyqtSlot()
    def lasso(self):
        self.log_action("Mode: lasso")
        print(self.sender().text())
        self.canva.set_mode(self.sender().text())    
        
    def log_action(self, str):
        content = self.textEdit.toPlainText()
        self.textEdit.setPlainText( content + "\n" + str)

def main(args):
    #print("Hello World")
    app = QApplication(args)
    win = MainWindow()


    win.show()

    app.exec_()

	
if __name__ == "__main__":
    main(sys.argv) 
    