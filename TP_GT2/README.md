# Travaux pratique - PyQt IHM

- Lyna Saoucha, Manel Khenifra
- Interaction Homme Machine 2020/2021
- Gilles Bailly

L'objectif de ce projet est de nous familiariser avec QT, une API qui offre des composants d'interface graphique et d'accès au données. Normalement implémentée en C++, nous avons utilisé le langage Python pour notre projet, grâce au module PyQt qui permet de lier Python et la bibliothèque Qt. 

Nous sommes allées jusqu'à l'étape du log. Certains morceaux de code ont pour commentaire l'étape du projet à laquelle ils correspondent. Vous pouvez trouvez ci dessous les réponses aux questions de l'énoncé ainsi que les options ajoutées.

## Semaine 2 : TP Qt 1

Toutes les étapes sont fonctionnelles, même celles non commentées ci dessous. En revanche, nous n'avons pas eu le temps de tester la plateforme QtDesigner.

### 1e Etape: Démarrage

Q1: Pour le code exécute, il ne faut pas oublier non plus de décommenter "import sys" en haut de la page.

### 2e Etape: Créer une classe *MainWindow*

Q2 : La fenêtre ne s'affiche pas car nous n'avons pas crée de QApplication pour exécuter. Il faut aussi rajouter la fonction super() qui hérite de la classe QMainWindow.

### 3e Etape: rajouter des widgets à MainWindow

- [x] Option 1 : Créer une barre de status en appelant la méthode *statusBar()* de la classe *QMainWindow*.

### 4e Etape: définir et connecter les slots

Q4 : On connecte les actions aux slots avec monAction.monSignal.connect(self.nomDuSlot)

### 6e Etape: ouvrir / sauver une page HTML

Q6 : On ne peut pas choisir l'extension du fichier lors de la sauvegarde.

### 9e etape

Nous avons ajouté un slider grâce à QSlider, à côté des différentes icônes de la barre de menu. Pour l'instant, ce slider affiche simplement les valeurs dans la console lorsqu'on glisse le curseur dessus.

## Semaine 3 - 4 : TME Qt

Les étapes 1 à 8 sont fonctionnelles. Nous voulions connecter le curseur fait précédent à un slot "zoom" qui permettrait de zoomer sur les formes lorsqu'on les séléctionne. Cette étape est en cours.

### 3e Etape: spécifier des attributs graphiques

- [x] Option: Nous avons ajouté la couleur du contour.
- [x] Nous avons aussi ajouté une option 'reset' dans la barre qui permet d'effacer notre dessin.

### 6e Etape: tracer plusieurs formes géométriques

Q1 :  On pourrait avoir une matrice (liste de liste) dans laquelle dans chaque case, on a la liste des formes correspondantes.

Ex: Forme\[1][0] = premier rectange | Forme\[1][1] = deuxieme rectangle

Forme\[2][0] = premier ellipse | Forme\[2][1] = deuxième ellipse

Chaque case de Forme correspond à un ensemble d'objets graphiques et dans chaque case on retrouve la liste des différents objets correspondant.

### 9e Etape: Log

On devrait logger :

- lorsqu'on ouvre ou sauvegarde un document, avec son nom

- lorsqu'on change de mode

- lorsqu'on change de forme

- lorsqu'on change la couleur du pinceau ou du contour.

On peut sauvegarder ce fichier avec l'option saveFile. 





