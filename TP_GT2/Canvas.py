from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *


class Canvas(QWidget):
    
	def __init__(self, parent = None):
		super(Canvas, self).__init__()
		print("class Canvas")
        
        #[TME2]1e Etape: Afficher une zone de dessin
		self.setMinimumSize(200, 200)
		self.begin = self.end = self.move = QPoint()
        
		self.form = "&Rectangle"
		self.mode = "&Draw"
		self.color = QColor("black")
		self.colorPen = QColor("black")
        
        #[TME2]6e Etape: tracer plusieurs formes géométriques
		self.rectList = []
		self.ellipList = []
		self.lineList = []
		self.points = []
		
		self.myBool = False
		self.cpt = 0
		self.idCurrentObj = 0
        
		self.zoomValue = 0


	def reset(self):
		print("reset")
		self.rectList = []
		self.ellipList = []
		self.lineList = []
		self.points = []
		return

	def add_object(self):
		print("add object")
    
    #[TME2]3e Etape: spécifier des attributs graphiques
	def set_color(self, color1):
		print("set color")
		self.color = color1
		if (self.mode == "&Select"):
			if (self.idCurrentObj != 0):
				for rect in self.rectList:
					if (rect[4] == self.idCurrentObj):
						self.cpt += 1
						self.rectList.append((rect[0], rect[1], color1, rect[3], self.cpt))
						
				for ellip in self.ellipList:
					if (ellip[4] == self.idCurrentObj):                      
						self.cpt += 1
						self.ellipList.append((ellip[0], ellip[1], color1, ellip[3], self.cpt))
						
				self.update()
		return

    
	def set_color_pen(self, color2):
		print("set color pen")
		self.colorPen = color2
		return
    
    #[TME2]3e Etape: spécifier des attributs graphiques
	def set_form(self, form1):
		self.form = form1
		if (self.mode == "&Select"):
			if (self.idCurrentObj != 0):
				for rect in self.rectList:
					if (rect[4] == self.idCurrentObj):
						self.cpt += 1
						self.rectList.remove((rect[0], rect[1], rect[2], rect[3], rect[4]))
						self.ellipList.append((rect[0], rect[1], rect[2], rect[3], self.cpt))
						
				for ellip in self.ellipList:
					if (ellip[4] == self.idCurrentObj):
						self.cpt += 1
						self.ellipList.remove((ellip[0], ellip[1], ellip[2], ellip[3], ellip[4]))
						self.rectList.append((ellip[0], ellip[1], ellip[2], ellip[3], self.cpt))
						
				self.update()
		return

    
    
	def set_mode(self, mode1):
		self.mode = mode1
		return
    
    #[TME2]2e Etape: Dessiner un rectangle interactivement
	def paintEvent(self, event):
		painter = QPainter(self)

        #[TME2]7 e Etape: Implementer plusieurs modes
		if (self.mode == "&Move"):
			painter.translate(self.move - self.begin)
		
        #[TME2]6e Etape: tracer plusieurs formes géométriques
		for rect in self.rectList:
			painter.setBrush(rect[2])
			painter.setPen(rect[3])
			painter.drawRect(QRect(rect[0], rect[1]))

		for ellip in self.ellipList:
			painter.setBrush(ellip[2])
			painter.setPen(ellip[3])
			painter.drawEllipse(QRect(ellip[0], ellip[1]))

		for line in self.lineList:
			painter.setBrush(line[1])
			painter.setPen(line[2])
			painter.drawPolyline(QPolygon(line[0]))          
       
        #[TME2]8 e Etape: modifier les attributs a posteriori
		if (self.myBool == True):
			pen = QPen(Qt.red)
			pen.setWidth(3)
			painter.setPen(pen)
            
			for rect in self.rectList:
				if (rect[4] == self.idCurrentObj):
					painter.setBrush(rect[2])
					painter.drawRect(QRect(rect[0], rect[1]))
                    

			for ellip in self.ellipList:
				if (ellip[4] == self.idCurrentObj):
					painter.setBrush(ellip[2])
					painter.drawEllipse(QRect(ellip[0], ellip[1]))
			for line in self.lineList:
				if (line[3] == self.idCurrentObj):
					painter.setPen(line[1])
					painter.drawPolyline(QPolygon(self.points))

			self.myBool = False
            
		if (self.mode == "&Draw"):
			painter.setBrush(self.color)
			painter.setPen(self.colorPen)
			if not (self.begin.isNull() and self.end.isNull()):
				if (self.form == "&Rectangle"):
					painter.drawRect(QRect(self.begin, self.end))
				elif (self.form == "&Ellipse"):
					painter.drawEllipse(QRect(self.begin, self.end))
				elif (self.form == "&Free drawing"):
					painter.drawPolyline(QPolygon(self.points))


			


	def mouseMoveEvent(self, event):
		self.points.append(event.pos())
		self.end = event.pos()

		if (self.mode == "&Move"):
			self.move = event.pos()

		self.update()


	def mousePressEvent(self, event):
		self.begin = self.end = event.pos()
		self.points.append(event.pos())

		if(self.mode == "&Move"):
			self.move = self.begin

		if (self.mode == "&Select"):
			for rect in self.rectList:
				if (QRect(rect[0], rect[1]).contains(event.pos())):
					self.myBool = True
					self.idCurrentObj = rect[4]
			for ellip in self.ellipList:
				if (QRect(ellip[0], ellip[1]).contains(event.pos())):
					self.myBool = True
					self.idCurrentObj = ellip[4]
			for line in self.lineList:
				if (QPolygon(line[0]).contains(event.pos())):
					self.myBool = True
					self.idCurrentObj = line[3]

		self.update()


    #[TME1]5e Etape: tracer plusieurs rectangles
	def mouseReleaseEvent(self, event):
		if (self.mode == "&Move"):
			for rec in self.rectList:
				rec[0] += self.move -self.begin
				rec[1] += self.move - self.begin
			for ell in self.ellipList:
				ell[0] += self.move - self.begin
				ell[1] += self.move - self.begin
			for line in self.lineList:
				line[0] = [line[0][i] + (self.move - self.begin) for i in range(len(line[0]))]
		
		if (self.mode == "&Draw"):
			self.cpt += 1
			if (self.form == "&Rectangle"):
				self.rectList.append([self.begin, self.end, self.color, self.colorPen, self.cpt])
			elif (self.form == "&Ellipse"):
				self.ellipList.append([self.begin, self.end, self.color, self.colorPen, self.cpt])
			elif (self.form == "&Free drawing"):
				self.lineList.append([self.points, self.color, self.colorPen, self.cpt])  

		self.begin = self.end = self.move = QPoint()
		self.points = []

		self.update()
  
	
