import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from Canvas import *
import resources


class MainWindow(QMainWindow):

    def __init__(self, parent = None):
        QMainWindow.__init__(self, parent)
        print( "init mainwindow")
        self.resize(600, 500)

        bar = self.menuBar()
        fileMenu = bar.addMenu("File")

        act1 = QAction(QIcon("open.png"), "Open...", self)
        act1.setShortcut( QKeySequence("Ctrl+O") )
        act1.setToolTip("Open file")
        act1.setStatusTip("Open file")

        act2 = QAction(QIcon("save.png"), "Save...", self)
        act2.setShortcut( QKeySequence("Ctrl+S") )
        act2.setToolTip("Save file")
        act2.setStatusTip("Save file")

        act3 = QAction(QIcon("quit.png"), "Quit...", self)
        act3.setShortcut( QKeySequence("Ctrl+Q") )
        act3.setToolTip("Quit")
        act3.setStatusTip("Quit")

        fileMenu.addAction(act1)
        fileMenu.addAction(act2)
        fileMenu.addAction(act3)

        colorMenu = bar.addMenu("Color")
        actPen = fileMenu.addAction(QIcon(":/icons/pen.png"), "&Pen color", self.pen_color, QKeySequence("Ctrl+P"))
        actBrush = fileMenu.addAction(QIcon(":/icons/brush.png"), "&Brush color", self.brush_color, QKeySequence("Ctrl+B"))

        fileToolBar = QToolBar("Fichier")
        fileToolBar.addAction(act1)
        fileToolBar.addAction(act2)
        fileToolBar.addAction(act3)
        self.addToolBar(fileToolBar)

        colorToolBar = QToolBar("Color")
        self.addToolBar( colorToolBar )
        colorToolBar.addAction( actPen )
        colorToolBar.addAction( actBrush )

        shapeMenu = bar.addMenu("Shape")
        actRectangle = fileMenu.addAction(QIcon(":/icons/rectangle.png"), "&Rectangle", self.rectangle )
        actEllipse = fileMenu.addAction(QIcon(":/icons/ellipse.png"), "&Ellipse", self.ellipse)
        actFree = fileMenu.addAction(QIcon(":/icons/free.png"), "&Free drawing", self.free_drawing)

        shapeToolBar = QToolBar("Shape")
        self.addToolBar( shapeToolBar )
        shapeToolBar.addAction( actRectangle )
        shapeToolBar.addAction( actEllipse )
        shapeToolBar.addAction( actFree )

        modeMenu = bar.addMenu("Mode")
        actMove = modeMenu.addAction(QIcon(":/icons/move.png"), "&Move", self.move)
        actDraw = modeMenu.addAction(QIcon(":/icons/draw.png"), "&Draw", self.draw)
        actSelect = modeMenu.addAction(QIcon(":/icons/select.png"), "&Select", self.select)

        modeToolBar = QToolBar("Navigation")
        self.addToolBar( modeToolBar )
        modeToolBar.addAction( actMove )
        modeToolBar.addAction( actDraw )
        modeToolBar.addAction( actSelect )
        
        statusBar = QStatusBar()
        self.setStatusBar(statusBar) 

        triggered = pyqtSignal()

        act1.triggered.connect(self.openFile)
        act2.triggered.connect(self.saveFile)
        act3.triggered.connect(self.quit)

        self.textEdit = QTextEdit() 
        self.canva = Canvas()
        
        container = QWidget()
        self.setCentralWidget(container);
       
        v_layout = QVBoxLayout(container)
        v_layout.addWidget(self.canva)
        v_layout.addWidget(self.textEdit)


    @pyqtSlot()
    def openFile(self):
        print("Opening file...")
        fileName = QFileDialog.getOpenFileName(self, "Open File", "/home", "Text files(*.txt);; HTML files (*.html)")
        print(fileName)  

        if (len(fileName[0]) != 0):
            file = open(fileName[0], "r")
            self.textEdit.setHtml(file.read())
            file.close()


    @pyqtSlot()
    def saveFile(self):
        print("Saving file...")	
        fileName = QFileDialog.getSaveFileName(self, "Save File")
        print(fileName) 

        if (len(fileName[0]) != 0):
            file = open(fileName[0], "w")
            file.write(self.textEdit.toHtml())
            file.close
	
    @pyqtSlot()
    def quit(self):	
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setText("Do you want to quit?")
        msg.setWindowTitle("Exit")
        msg.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        answer = msg.exec_()
        if (answer == QMessageBox.Yes):
            self.close()
        
    @pyqtSlot()
    def pen_color(self):
        self.log_action("choose pen color")
    
    @pyqtSlot()
    def brush_color(self):
        self.log_action("choose brush color")
        color = QColorDialog().getColor()
        self.canva.set_color(color)

    @pyqtSlot()
    def rectangle(self):
        self.log_action("Shape mode: rectangle")
        self.canva.set_form(self.sender().text())

    @pyqtSlot()
    def ellipse(self):
        self.log_action("Shape Mode: circle")
        self.canva.set_form(self.sender().text())

    def free_drawing(self):
        self.log_action("Shape mode: free drawing")

    @pyqtSlot()
    def move(self):
        self.log_action("Mode: move")
        self.canva.set_mode(self.sender().text())

    @pyqtSlot()
    def draw(self):
        self.log_action("Mode: draw")

    @pyqtSlot()
    def select(self):
        self.log_action("Mode: select")

    def log_action(self, str):
        content = self.textEdit.toPlainText()
        self.textEdit.setPlainText( content + "\n" + str)


def main(args):
    print("Hello World")
    app = QApplication(args)
    win = MainWindow()

    win.show()
    app.exec_()

	
if __name__ == "__main__":
    main(sys.argv) 
    
