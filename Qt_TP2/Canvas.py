from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *


class Canvas(QWidget):

	def __init__(self, parent = None):
		super(Canvas, self).__init__()
		print("class Canvas")

		self.setMinimumSize(200, 200)
		self.begin = self.end = QPoint()
		self.form = "&Rectangle"
		self.color = QColor("black")
		self.rectList = []
		self.ellipList = []
		self.mode = "&Draw"


	def reset(self):
		print("reset")

	def add_object(self):
		print("add object")

	def set_color(self, color1):
		print("set color")
		self.color = color1
		return

	def set_form(self, form1):
		self.form = form1
		return

	def set_mode(self, mode1):
		self.mode = mode1
		return

	def paintEvent(self, event):
		painter = QPainter(self)
		painter.setPen(Qt.black)

		for rect in self.rectList:
			painter.setBrush(rect[2])
			painter.drawRect(QRect(rect[0], rect[1]))

		for ellip in self.ellipList:
			painter.setBrush(ellip[2])
			painter.drawEllipse(QRect(ellip[0], ellip[1]))

		if (self.mode == "&Draw"):
			painter.setBrush(self.color)
			if not (self.begin.isNull() and self.end.isNull()):
				if (self.form == "&Rectangle"):
					painter.drawRect(QRect(self.begin, self.end))
				elif (self.form == "&Ellipse"):
					painter.drawEllipse(QRect(self.begin, self.end))

		#elif (self.mode == "&Move"):
			#painter.translate(self.begin)

	def mouseMoveEvent(self, event):
		self.end = event.pos()
		self.update()

	def mousePressEvent(self, event):
		self.begin = self.end = event.pos()
		self.update()

	def mouseReleaseEvent(self, event):
		if (self.mode == "&Draw"):
			if (self.form == "&Rectangle"):
				self.rectList.append((self.begin, self.end, self.color))
			elif (self.form == "&Ellipse"):
				self.ellipList.append((self.begin, self.end, self.color))
			self.begin = self.end = QPoint()
			self.update()
		#elif (self.mode == "&Move"):
			




		
	
